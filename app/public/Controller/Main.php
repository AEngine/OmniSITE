<?php

namespace Controller;

use AEngine\Orchid\Controller;
use AEngine\Orchid\Http\Request;
use AEngine\Orchid\Http\Response;
use AEngine\Orchid\View;

class Main extends Controller
{
    public function index(Request $request, Response $response)
    {
        return View::fetch(app()->path('view:Layout.php'));
    }
}
